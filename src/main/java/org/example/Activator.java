package org.example;

import java.io.IOException;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

    ScheduledExecutorService ses = null;

    @Override
    public void start(BundleContext bundleContext) throws Exception {

        System.out.println("Send Bitfinex: start v2");

        BfxHttpClientGet bfx = new BfxHttpClientGet();
        this.ses = Executors.newScheduledThreadPool(1);
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));

        Runnable task = new Runnable() {

            BfxHttpClientGet bfx = new BfxHttpClientGet();
            int key = 0;
            public void run() {
                try {
                    ++key;
                    action(bfx, key);
                    if (key == 2) {
                        key = 0;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        ScheduledFuture<?> scheduledFuture = ses.scheduleWithFixedDelay(task, 10, 2, TimeUnit.SECONDS);
    }


    private static void action(BfxHttpClientGet bfx, int key) throws IOException {

        String lable = "BTC";
        String REQ1 = "https://api.bitfinex.com/v1/book/" + lable + "usd?limit_bids=50000&limit_asks=50000";
        String REQ2 = "https://api.bitfinex.com/v1/trades/" + lable + "usd?limit_trades=990";

        if (key == 1) {
            boolean Req1 = bfx.BfxReq1(REQ1, key);
        }
        if (key == 2) {
            boolean Req2 = bfx.BfxReq1(REQ2, key);
        }
    }


    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        System.out.println("Send Bitfinex: stop");
        try {
            this.ses.shutdownNow();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}