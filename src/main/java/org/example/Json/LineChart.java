
package org.example.Json;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class LineChart {

    private float date;
    private Long units;

}
