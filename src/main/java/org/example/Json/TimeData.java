package org.example.Json;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@SuppressWarnings("unused")

public class TimeData {

    private float maxPrice;
    private float minPrice;
    private float price;
    private float fstPrice;
    private float endPrice;
    private float h1;
    private float l1;
    private float mask;
    private float mbid;
    private float ask;
    private float bid;
    private float ord;
    private long eproch;
}


