package org.example;

import org.example.Json.LineChart;
import org.example.pojo.DataWriteDb;
import org.example.pojo.TimeData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.math.DoubleMath;
import org.sqlite.SQLiteDataSource;

import java.io.IOException;
import java.nio.file.*;
import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;


public class DataBase {

    String tabName = "BTC";
    String NameMinuts = "Minuts";
    String place = "./Database/Bitfinex/";
    Connection conn = null;

    private String getLableData() {

        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        // System.setProperty("+00:00:00", "EST");
        Instant instant = Instant.now();
        ZoneId z = ZoneId.of("Asia/Yekaterinburg"); //"Asia/Yekaterinburg"+05:00:00
        ZonedDateTime zdt = instant.atZone(z);
        String[] subStr = zdt.toString().split("T");
        return subStr[0].replace("-", "_");

    }

    private Long getEproch() {

        Instant instant = Instant.now();
        return instant.toEpochMilli();
    }


    public String readDB(int cnt) {

        String dbName = getLableData();
        long epoch = getEproch();

        String url = "jdbc:sqlite:" + place + dbName + ".db";

        String rezult = "";

        SQLiteDataSource ds = new SQLiteDataSource();
        ds.setUrl(url);

        try (Connection conn = ds.getConnection()) {
            //System.out.println("Connected.");
            Statement stmt = conn.createStatement();
            String TABLE = tabName + dbName;
            String sql = "SELECT * FROM " + TABLE;
            ResultSet rs = stmt.executeQuery(sql);
            // System.out.println("Data read...");
            List<LineChart> lCh = new ArrayList<>();

            while (rs.next()) {
                LineChart ln = new LineChart();
                ln.setDate(rs.getFloat("price"));
                ln.setUnits(rs.getLong("eproch"));
                lCh.add(ln);
            }

            rs.close();
            stmt.close();
            conn.close();

            rezult += "[ " + '\n';
            for (int i = 0; i < lCh.size(); i++) {

                ObjectMapper Obj = new ObjectMapper();
                String jsonStr = Obj.writeValueAsString(lCh.get(i));
                rezult += jsonStr;

                if (i != lCh.size() - 1) {
                    rezult += ", \n";
                }

            }

            rezult += '\n';
            rezult += " ]";
            //System.out.println("Read complite..." + rezult);
            return rezult;

        } catch (SQLException | JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return rezult;
    }


    public boolean writeMinutsDatatoBd(List<DataWriteDb> readData) {

        boolean rezult = false;

        float summa_Ask = 0, summa_Bid = 0;
        long ep = 0;

        List<Float> Price = new LinkedList<Float>();
        List<Float> L1 = new LinkedList<Float>();
        List<Float> H1 = new LinkedList<Float>();
        List<Float> Ord = new LinkedList<Float>();

        List<Float> midAsks = new LinkedList<Float>();
        List<Float> midBids = new LinkedList<Float>();

        for (int i = 0; i < readData.size(); i++) {

            if (i > 0) {
                summa_Ask += (readData.get(i).getAsk() - readData.get(i - 1).getAsk());
                summa_Bid += (readData.get(i).getBid() - readData.get(i - 1).getBid());
                //System.out.println("summa_Ask: " +(readData.get(i).getAsk()  - readData.get(i - 1).getAsk()) );
                //System.out.println("readData: " + readData.size());
            }


            midAsks.add(readData.get(i).getAsk());
            midBids.add(readData.get(i).getBid());

            Price.add(readData.get(i).getPrice());
            Ord.add(readData.get(i).getOrd());
            L1.add(readData.get(i).getL1());
            H1.add(readData.get(i).getH1());
        }

        TimeData td = new TimeData();

        td.setFstPrice(Price.get(0));
        td.setEndPrice(Price.get(Price.size() - 1));
        td.setMaxPrice(Collections.max(Price));
        td.setMinPrice(Collections.min(Price));
        td.setPrice((float) DoubleMath.mean(Price));
        td.setH1((float) Collections.max(H1));
        td.setL1((float) Collections.min(L1));

        td.setMbid((float) DoubleMath.mean(midBids));
        td.setMask((float) DoubleMath.mean(midAsks));

        td.setAsk(summa_Ask);
        td.setBid(summa_Bid);
        td.setOrd((float) DoubleMath.mean(Ord));

        td.setEproch(getEproch());

        //  System.out.println("writeDatatoBd: " + wrDb.toString());
        //   System.out.println("----------------------------------" + (new Date()));

        if (ExistFileDb()) {

            String dbName = getLableData();
            long epoch = getEproch();

            String url = "jdbc:sqlite:" + place + dbName + ".db";
            String REQ = "INSERT INTO " + tabName + NameMinuts + dbName + " (FstPrice, EndPrice, MaxPrice, MinPrice, Price, L1, H1, Mbid, Mask, Ask, Bid, Ord, Eproch) VALUES (" + td.getFstPrice() + ", " + td.getEndPrice() + ", " + td.getMaxPrice() + ", " + td.getMinPrice() + ", " + td.getPrice() + ", " + td.getL1() + ", " + td.getH1() + ", " + td.getMbid() + ", " + td.getMask() + ", " + summa_Ask + ", " + summa_Bid + ", " + td.getOrd() + ", " + td.getEproch() + ");";

            SQLiteDataSource ds = new SQLiteDataSource();
            ds.setUrl(url);

            try (Connection conn = ds.getConnection()) {

                Statement stmt = conn.createStatement();
                stmt.execute(REQ);

                stmt.close();
                conn.close();
                rezult = true;
               // System.out.println("writeDatatoBd: execute");

            } catch (SQLException e) {
                System.out.println(e.getMessage());

                createMinutsTable();
            }

        } else {
            //  System.out.println("Create Db");
            createMinutsTable();
        }

        return rezult;


    }

    //----------------------------------------------------------------------

    public boolean writeDatatoBd(DataWriteDb wrDb) {

        boolean rezult = false;

        //  System.out.println("writeDatatoBd: " + wrDb.toString());
        //   System.out.println("----------------------------------" + (new Date()));

        if (ExistFileDb()) {

            String dbName = getLableData();
            long epoch = getEproch();

            String url = "jdbc:sqlite:" + place + dbName + ".db";
            String REQ = "INSERT INTO " + tabName + dbName + " (price, l1, h1, l2, h2, l3, h3, bid, ask, eproch, ord) VALUES (" + wrDb.getPrice() + ", " + wrDb.getL1() + ", " + wrDb.getH1() + ", " + wrDb.getL2() + ", " + wrDb.getH2() + ", " + wrDb.getL3() + ", " + wrDb.getH3() + ", " + wrDb.getBid() + ", " + wrDb.getAsk() + ", " + epoch + ", " + wrDb.getOrd() + ");";

            //System.out.println("REQ : " + REQ);
            // System.out.println("url: " + url);

            SQLiteDataSource ds = new SQLiteDataSource();
            ds.setUrl(url);

            try (Connection conn = ds.getConnection()) {

                Statement stmt = conn.createStatement();
                stmt.execute(REQ);

                stmt.close();
                conn.close();
                rezult = true;
                System.out.println("Send Bitfinex: write data to bd: execute");

            } catch (SQLException e) {
                System.out.println("ERRROR: Send Bitfinex: write to db " + e.getMessage());
                //DeleteFileDb();
                createTable(wrDb);
            }

        } else {
            //DeleteFileDb();
            createTable(wrDb);
        }

        return rezult;
    }

//--------------------------------------------------------------

    public boolean createMinutsTable() {

        boolean rezult = false;

        String dbName = getLableData();
        long epoch = getEproch();

        String REQ = "CREATE TABLE IF NOT EXISTS " + tabName + NameMinuts + dbName + " (id INTEGER PRIMARY KEY, FstPrice FLOAT, EndPrice FLOAT, MaxPrice FLOAT, MinPrice FLOAT, Price FLOAT, L1 FLOAT, H1 FLOAT, Mbid FLOAT, Mask FLOAT, Ask FLOAT, Bid FLOAT, Ord FLOAT, Eproch INTEGER, time DATETIME DEFAULT CURRENT_TIMESTAMP);";


        System.out.println("createTable: " + REQ);

        String url = "jdbc:sqlite:" + place + dbName + ".db";
        //   System.out.println("url: " + url);

        SQLiteDataSource ds = new SQLiteDataSource();
        ds.setUrl(url);

        try (Connection conn = ds.getConnection()) {

            Statement stmt = conn.createStatement();
            stmt.execute(REQ);

            stmt.close();
            conn.close();
            rezult = true;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }


        return rezult;
    }


//----------------------------------------------------------------


    public boolean createTable(DataWriteDb wrDb) {

        boolean rezult = false;

        String dbName = getLableData();
        long epoch = getEproch();

        String REQ = "CREATE TABLE IF NOT EXISTS " + tabName + dbName + " "
                + "(id INTEGER PRIMARY KEY, price FLOAT, l1 FLOAT, h1 FLOAT, l2 FLOAT, h2 FLOAT, l3 FLOAT, h3 FLOAT, bid FLOAT, ask FLOAT, eproch INTEGER, ord FLOAT,  time DATETIME DEFAULT CURRENT_TIMESTAMP);";
        String REQ1 = "INSERT INTO " + tabName + dbName + " (price, l1, h1, l2, h2, l3, h3, bid, ask, eproch, ord) VALUES (" + wrDb.getPrice() + ", " + wrDb.getL1() + ", " + wrDb.getH1() + ", " + wrDb.getL2() + ", " + wrDb.getH2() + ", " + wrDb.getL3() + ", " + wrDb.getH3() + ", " + wrDb.getBid() + ", " + wrDb.getAsk() + ", " + epoch + ", " + wrDb.getOrd() + ");";

        System.out.println("createTable: " + REQ);

        String url = "jdbc:sqlite:" + place + dbName + ".db";
        //   System.out.println("url: " + url);

        SQLiteDataSource ds = new SQLiteDataSource();
        ds.setUrl(url);

        try (Connection conn = ds.getConnection()) {

            Statement stmt = conn.createStatement();
            stmt.execute(REQ);
            stmt.execute(REQ1);

            stmt.close();
            conn.close();
            rezult = true;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }


        return rezult;
    }


    public boolean connection() {

        boolean rezult = false;

        try {

            String dbName = getLableData();

            String url = "jdbc:sqlite:" + place + dbName + ".db";

            SQLiteDataSource ds = new SQLiteDataSource();
            ds.setUrl(url);

            try (Connection conn = ds.getConnection()) {

                //    System.out.println("Connection to SQLite has been established.");
                conn.close();
                return true;
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return rezult;
    }


    public boolean ExistFileDb() {

        String dbName = getLableData();
        String part = place + dbName + ".db";
        final Path path = Paths.get(part);
        return Files.exists(path);
    }


    public boolean DeleteFileDb() {

        boolean rezult = false;

        String[] subStr = LocalDateTime.now().atZone(ZoneId.systemDefault()).toString().split("T");
        String dbName = subStr[0].replace("-", "_");

        String part = place + dbName + ".db";

        try {
            Files.deleteIfExists(Paths.get(part));
            rezult = true;
        } catch (NoSuchFileException e) {
            System.out.println("No such file/directory exists");
        } catch (DirectoryNotEmptyException e) {
            System.out.println("Directory is not empty.");
        } catch (IOException e) {
            System.out.println("Invalid permissions.");
        }

        System.out.println("Deletion successful.");

        return rezult;
    }
}


