
package org.example.pojo;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class Bid {

    private String amount;
    private String price;
    private String timestamp;

}
