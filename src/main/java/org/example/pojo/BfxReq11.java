package org.example.pojo;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class BfxReq11 {

    private String amount;
    private String price;
    private String timestamp;

}