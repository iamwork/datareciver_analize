
package org.example.pojo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@ToString
@Data
@SuppressWarnings("unused")
public class BfxReq1 {

    private List<Ask> asks;
    private List<Bid> bids;

}
