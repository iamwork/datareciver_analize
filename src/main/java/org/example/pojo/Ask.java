
package org.example.pojo;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class Ask {

    private String amount;
    private String price;
    private String timestamp;


}
