package org.example.pojo;

import lombok.Data;
import lombok.ToString;


@ToString
@Data
@SuppressWarnings("unused")
public class DataWriteDb {

    private float price ;
    private float l1 ;
    private float h1 ;
    private float l2 ;
    private float h2 ;
    private float l3 ;
    private float h3 ;
    private float bid ;
    private float ask ;
    private float ord ;

}


