
package org.example.pojo;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
@SuppressWarnings("unused")
public class BfxReq2 {

    private String amount;
    private String exchange;
    private String price;
    private long tid;
    private long timestamp;
    private String type;

}
