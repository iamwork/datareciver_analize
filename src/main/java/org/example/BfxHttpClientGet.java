package org.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.pojo.BfxReq1;
import org.example.pojo.BfxReq2;
import org.example.pojo.DataWriteDb;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class BfxHttpClientGet {

    DataWriteDb wrDb = new DataWriteDb();
    List <DataWriteDb> AllWdB = new ArrayList<>();
    float oldAsk = 0, oldBid = 0;
    List<Float> Asks_H = new ArrayList<Float>();
    List<Float> Bids_L = new ArrayList<Float>();
    List<Float> Asks_H_Max = new ArrayList<Float>();
    List<Float> Bids_L_Max = new ArrayList<Float>();
    List<Integer> inp = new ArrayList<Integer>();
    List<Integer> output = new ArrayList<Integer>();
    List<String> DataList = new ArrayList<String>();
    int cnt_rq = 0;

    public boolean BfxReq1(String REQ1, int key) throws IOException {

        boolean rezult = false;
        StringBuilder resp = new StringBuilder();
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(REQ1);
        HttpResponse response = client.execute(request);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line = "";
        while ((line = rd.readLine()) != null) {
            resp.append(line);
        }
        if (!resp.toString().equals( "{ \"error\": \"ERR_RATE_LIMIT\"}")
        ){

        if (key == 1) {
            BfxReq1GetData(resp.toString());
        }
        if (key == 2) {
            BfxReq2GetData(resp.toString());

            if ((wrDb.getPrice() > 0) &&(wrDb.getAsk() > 0) && (wrDb.getBid() > 0)) {
                 DataBase app = new DataBase();
                if ((oldAsk != wrDb.getAsk()) || (oldBid != wrDb.getBid())) {
                    oldAsk = wrDb.getAsk();
                    oldBid = wrDb.getBid();
                    app.writeDatatoBd(wrDb);
                }
            }
        }
        rezult = true;
        }else System.out.println("Send Bitfinex: ERR_RATE_LIMIT!" );
        return rezult;
    }

    private int findMax(List<Float> Data){
        float max = Data.get(0);
        int position = 0;
        for (int i = 0; i < Data.size(); i++) {
            if (Data.get(i) > max) {
                max = Data.get(i);
                position = i;
            }
        }
        return position;
    }


    private List<Float>  calcLayerAsks(BfxReq1 obj, float koef, int layer) throws IOException {

        int price = 0;
        int amount = 1;
        float summa = 0;
        Float ask_price = Float.valueOf(obj.getAsks().get(0).getPrice());
        float koef_H = 1 + ((float) 0.001 * koef) * layer;
        float koef_L = 1 + (((float) 0.001 * koef) * (layer - 1));
        List<Float>  data = new ArrayList<>();

        for (int i = 0; i < obj.getAsks().size(); i++) {
            if (
                    (Float.valueOf(obj.getAsks().get(i).getPrice()) < (ask_price * koef_H))
                            &&
                            (Float.valueOf(obj.getAsks().get(i).getPrice()) > (ask_price * koef_L))
            ) {
                summa += Float.valueOf(obj.getAsks().get(i).getAmount());
            }
        }
        //  System.out.println(cnt + " /" + koef+ "  /" + "summa: " + summa + "  price > " + ask_price * koef_L  +  "  price < " + ask_price * koef_H  );
        data.add (ask_price * koef_L);
        data.add(summa);

        return  data;
    }


    private List<Float> calcLayerBids(BfxReq1 obj, float koef, int layer) throws IOException {

        int price = 0;
        int amount = 1;
        float summa = 0;

        Float bid_price = Float.valueOf(obj.getBids().get(0).getPrice());

        float koef_L = 1 - ((float) 0.001 * koef) * layer;
        float koef_H = 1 - (((float) 0.001 * koef) * (layer - 1));

        List<Float>  data = new ArrayList<>();

        for (int i = 0; i < obj.getBids().size(); i++) {

            if (
                    (Float.valueOf(obj.getBids().get(i).getPrice()) < (bid_price * koef_H))
                            &&
                            (Float.valueOf(obj.getBids().get(i).getPrice()) > (bid_price * koef_L))
            ) {
                summa += Float.valueOf(obj.getBids().get(i).getAmount());
            }
        }
        //  System.out.println( " /" + koef+ "  /" + "summa: " + summa + "  price > " + bid_price * koef_L  +  "  price < " + bid_price * koef_H  );
        data.add (bid_price * koef_H);
        data.add(summa);

        return  data;
    }


    private List<Float> FindLayers( BfxReq1 obj, int size) throws IOException {

        List<Float> summa_Asks_H_op = new ArrayList<Float>();
        List<Float> summa_Bids_L_op = new ArrayList<Float>();
        List<Float> Asks_H_op = new ArrayList<Float>();
        List<Float> Bids_L_op = new ArrayList<Float>();
        List<Float>  data_hl = new ArrayList<>();
        int price = 0;
        int amount = 1;
        float k = 1;

        for (int i = 1; i < size; i++) {
            summa_Asks_H_op.add((calcLayerAsks(obj, k, i)).get(amount));
            Asks_H_op.add((calcLayerAsks(obj, k, i)).get(price));
        }

        for (int i = 1; i < size; i++) {
            summa_Bids_L_op.add((calcLayerBids(obj, k, i)).get(amount));
            Bids_L_op.add((calcLayerBids(obj, k, i)).get(price));
        }

        int max_levelAsks = findMax(summa_Asks_H_op);
        int max_levelBids = findMax(summa_Bids_L_op);
        //---
        float sb = 0, sa = 0;
        for (int i = 0; i < max_levelBids + 1; i++) {
            sb += summa_Bids_L_op.get(i);
        }
        for (int i = 0; i < max_levelAsks + 1; i++) {
            sa += summa_Asks_H_op.get(i);
        }
        float ord = sb / sa;

        data_hl.add(Bids_L_op.get(max_levelBids));
        data_hl.add(Asks_H_op.get(max_levelAsks));
        data_hl.add(ord);
        //System.out.println("ord:" + ord);

        return data_hl;
    }


    private void BfxReq1GetData(String resp) throws IOException{

        if (resp.length() > 0) {

            final ObjectMapper objectMapper = new ObjectMapper();
            final BfxReq1 obj = objectMapper.readValue(resp, BfxReq1.class);
            int price = 0;
            int amount = 1;
            wrDb.setPrice((Float.parseFloat(obj.getBids().get(1).getPrice()) + Float.parseFloat(obj.getAsks().get(1).getPrice())) / (float) 2);    // 1

            List<Float>  data_hl =  FindLayers( obj, 51);
            wrDb.setL1(data_hl.get(0));
            wrDb.setH1(data_hl.get(1));
            wrDb.setOrd(data_hl.get(2));

            data_hl =  FindLayers( obj, 21);
            wrDb.setL2(data_hl.get(0));
            wrDb.setH2(data_hl.get(1));

            data_hl =  FindLayers( obj, 101);
            wrDb.setL3(data_hl.get(0));
            wrDb.setH3(data_hl.get(1));

        }

    }


    private void BfxReq2GetData(String resp) throws IOException {

        char[] array = resp.toCharArray();
        boolean start = false;
        int ind = 0;
        int ind_open = 0;
        int ind_closed = 0;
        inp.clear();
        output.clear();
        DataList.clear();

        for (char ch : array) {
            if (ch == '{') {
                start = true;
                inp.add(ind);
                ind_open++;
            }

            if (ch == '}') {
                start = false;
                output.add(ind);
                ind_closed++;
            }
            ind++;
        }

        if (ind_open == ind_closed) {

            for (int i = 0; i < inp.size(); i++) {
                int size = output.get(i) - inp.get(i);
                char[] data = new char[size + 1];
                int b = 0;
                for (int a = inp.get(i); a < output.get(i) + 1; a++) {
                    data[b] = array[a];
                    b++;
                }
                String string = new String(data);
                DataList.add(string);
            }

        } else {
            System.out.println("Send Bitfinex: Req2 resive data is not complite!");
        }

        if (DataList.size() == 990) {
            final ObjectMapper objectMapper = new ObjectMapper();
            final List<BfxReq2> classes_rq2 = new ArrayList<BfxReq2>();

            for (int i = 0; i < DataList.size(); i++) {
                classes_rq2.add(objectMapper.readValue(DataList.get(i), BfxReq2.class));
            }

            float summa_buy = 0, summa_sell = 0;

            for (int i = 0; i < classes_rq2.size(); i++) {
                if (classes_rq2.get(i).getType().equals("buy")) {
                    summa_buy += Float.valueOf(classes_rq2.get(i).getAmount());
                }
                if (classes_rq2.get(i).getType().equals("sell")) {
                    summa_sell += Float.valueOf(classes_rq2.get(i).getAmount());
                }
            }
            wrDb.setAsk(summa_sell);
            wrDb.setBid(summa_buy);
        }

    }
}
